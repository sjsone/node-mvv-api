import { LocationType } from "./Location";

export interface RawLocation {
    type: LocationType,
    latitude: number,
    longitude: number,
    id: string,
    divaId: number,
    place: string,
    name: string,
    hasLiveData: boolean,
    hasZoomData: boolean,
    products: string[],
    tariffZones: string,
    lines: {
        tram: any[],
        nachttram: any[],
        sbahn: any[],
        ubahn: any[],
        bus: any[],
        nachtbus: any[],
        otherlines: any[]
    }
}