
import { uriRequest, factoryLocation } from './Utils'
import { Station } from './Station'
import { Route, Connection } from './Route'

import { RawRoutesResult } from './RawRoutesResult'
import { RawLocationQueryResult } from './RawLocationQueryResult'
import { LocationType } from './Location'
import { Context, ContextData } from './Context'

export { Route, Connection }
export { Station }

export interface SearchOptions {
    web?: boolean
    type?: LocationType
    products?: string[]
}

export class MvvApi {
    protected context = new Context()

    constructor(contextData: ContextData = {}) {
        if(contextData.baseUri) {
            this.context.baseUri = contextData.baseUri
        }
        if(contextData.enviroment) {
            this.context.enviroment = contextData.enviroment
        }
    }

    /**
     * @param id 
     * @returns 
     */
    async station(id: string) {
        const result = <RawLocationQueryResult>await uriRequest('location/query', this.context,{ q: id })
        const stationRaw = result.locations.filter(r => r.id === id)[0]
        if (!stationRaw) {
            return undefined
        }
        return Station.FromRaw(stationRaw)
    }

    /**
     * @param query 
     * @param options 
     * @returns {Promise<Location[]>}
     */
    async search(query: string, options: SearchOptions = { web: false }) {
        const path = options && options.web ? 'location/queryWeb' : 'location/query'
        const result = <RawLocationQueryResult>await uriRequest(path, this.context, { q: query })

        let locations = result.locations
        if (options && options.type) {
            locations = locations.filter(l => l.type === options.type)
        }
        if (options && Array.isArray(options.products)) {
            locations = locations.filter(l => l.products.some(p => options.products?.includes(p)))
        }
        return locations.map(l => factoryLocation(l))
    }

    /**
     * 
     * @param {Station} from 
     * @param {Station} to 
     */
    async routes(from: Station, to: Station) {
        const result = <RawRoutesResult>await uriRequest('routing', this.context, { fromStation: from.id, toStation: to.id })
        return result.connectionList.map(c => Route.FromRaw(c))
    }
}