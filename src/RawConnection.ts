import { RawLocation } from "./RawLocation";

export interface RawConnectionPartPath {
    type: string,
    latitude: number,
    longitude: number
}

export interface RawConnectionPartStop {
    location: RawLocation,
    time: number, // timestamp
    delay: number
    arrDelay: number
    cancelled: boolean
}

export interface RawConnectionPathDescription {
    from: number,
    to: number,
    level: number
}

export interface RawConnection {
    stops: RawConnectionPartStop[],
    from: RawLocation,
    to: RawLocation,
    path: RawConnectionPartPath[],
    pathDescription: RawConnectionPathDescription[], 
    interchangePath: RawConnectionPartPath[], 
    departure: number, // timestamp
    arrival: number, // timestamp
    cancelled: boolean,
    product: string,
    label: string,
    connectionPartType: string,
    serverId: string,
    destination: string,
    sev: boolean,
    zoomNoticeDeparture: boolean,
    zoomNoticeArrival: boolean,
    zoomNoticeDepartureEscalator: boolean,
    zoomNoticeArrivalEscalator: boolean,
    zoomNoticeDepartureElevator: boolean,
    zoomNoticeArrivalElevator: boolean,
    departurePlatform: string,
    departureStopPositionNumber: number,
    arrivalPlatform: string,
    arrivalStopPositionNumber: number,
    noChangingRequired: boolean,
    fromId: string,
    departureId: string
    infoMessages?: string[]
}