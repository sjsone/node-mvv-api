
export const BaseUri = 'https://www.mvg.de/api/fahrinfo'


export interface ContextData {
    baseUri?: string
    enviroment?: ContextEnviroment
}

export enum ContextEnviroment {
    Node = 'env_node',
    Browser = 'env_browser'
}

export class Context implements ContextData {

    public baseUri: string = BaseUri
    public enviroment: ContextEnviroment = ContextEnviroment.Node
}