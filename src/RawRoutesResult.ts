import { RawConnection } from './RawConnection';
import { RawLocation } from './RawLocation'


export interface RawRouteConnection {
    zoomNoticeFrom: boolean,
    zoomNoticeTo: boolean,
    zoomNoticeFromEscalator: boolean,
    zoomNoticeToEscalator: boolean,
    zoomNoticeFromElevator: boolean,
    zoomNoticeToElevator: boolean,
    from: RawLocation,
    to: RawLocation,
    departure: number, // timestamp
    arrival: number, // timestamp
    connectionPartList: RawConnection[]
}

export interface RawRoutesResult {
    connectionList: RawRouteConnection[]
}