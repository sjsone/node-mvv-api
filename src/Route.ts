
import { RawConnection } from './RawConnection'
import { RawRouteConnection } from './RawRoutesResult'
import { factoryLocation } from './Utils'
import { Location } from './Location'

export interface ConnectionDeparture {
    date: Date
    platform: string
}

export interface ConnectionArrival {
    date: Date
    platform: string
}


export class Connection {

    static TYPE = {
        TRANSPORTATION: 'TRANSPORTATION',
        FOOTWAY: 'FOOTWAY'
    }

    // TODO: protected stops: any[] = []
    protected from: Location
    protected to: Location
    protected departure: ConnectionDeparture
    protected arrival: ConnectionArrival
    protected product: string = ''
    protected label: string = ''
    protected type: string = ''

    constructor(from: Location, to: Location, departure: ConnectionDeparture, arrival: ConnectionArrival) {
        this.from = from
        this.to = to
        this.departure = departure
        this.arrival = arrival
    }

    static FromRaw(raw: RawConnection) {
        const from = factoryLocation(raw.from)
        const to = factoryLocation(raw.to)
        const departure = {
            date: new Date(raw.departure),
            platform: raw.departurePlatform
        }
        const arrival = {
            date: new Date(raw.arrival),
            platform: raw.arrivalPlatform
        }
        const connection = new Connection(from, to, departure, arrival)
        connection.product = raw.product
        connection.label = raw.label
        connection.type = raw.connectionPartType
        return connection
    }
}

export class Route {

    protected from: Location
    protected to: Location
    protected departure: Date
    protected arrival: Date
    protected connections: Connection[] = []

    constructor(from: Location, to: Location, departure: Date, arrival: Date) {
        this.from = from
        this.to = to
        this.departure = departure
        this.arrival = arrival
    }

    static FromRaw(raw: RawRouteConnection) {
        const from = factoryLocation(raw.from)
        const to = factoryLocation(raw.to)
        const departure = new Date(raw.departure)
        const arrival = new Date(raw.arrival)
        const route = new Route(from, to, departure, arrival)

        route.connections = raw.connectionPartList.map(raw => Connection.FromRaw(raw))

        return route
    }
}