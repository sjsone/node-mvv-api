import { RawLocation } from "./RawLocation";

export interface RawLocationQueryResult {
    locations: RawLocation[]
}