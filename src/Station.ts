import { Location } from './Location'

export class Station extends Location {
    static TYPE = 'station'
    static PROD = {
        BUS: 'BUS',
        UBAHN: 'UBAHN',
        SBAHN: 'SBAHN',
        BAHN: 'BAHN',
        TRAM: 'TRAM'
    }

    public id: string = ''
    public name: string = ''
    public place: string = ''
    public products: string[] = []
}