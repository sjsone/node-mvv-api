import { RawLocation } from "./RawLocation";
import { StringIndexable } from "./Utils";

export type LocationType = 'station' | 'address'

export class Location {

    protected type: LocationType;
    protected latitude: number;
    protected longitude: number;
    protected place: string;

    constructor() {
        this.type = 'station';
        this.latitude = -1;
        this.longitude = -1;
        this.place = '';
    }

    getType() {
        return this.type
    }
    setType(type: LocationType) {
        this.type = type
    }

    getLatitude() {
        return this.latitude
    }
    setLatitude(latitude: number) {
        this.latitude = latitude
    }

    getLongitude() {
        return this.longitude
    }
    setLongitude(longitude: number) {
        this.longitude = longitude
    }

    getPlace() {
        return this.place
    }
    setPlace(place: string) {
        this.place = place
    }

    static FromRaw(raw: RawLocation, exclude: string[] = []) {
        const entity = new this()
        for (const key in entity) {
            if (Object.keys(raw).includes(key) && !exclude.includes(key)) {
                (<StringIndexable>entity)[key] = (<StringIndexable>raw)[key]
            }
        }
        return entity
    }
}