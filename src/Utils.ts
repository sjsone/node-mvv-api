import * as NodeHttps from 'https'

import { Location } from './Location'
import { Station } from './Station'
import { RawLocation } from './RawLocation'
import { Context, ContextEnviroment } from './Context'

const BASE_URI = 'https://www.mvg.de/api/fahrinfo'

export type StringIndexable = { [key: string]: any }

const joinPaths = (paths: string[]) => paths.join('/').replace(new RegExp('/{1,}', 'g'), '/');

export function nodeRequest(url: URL, options = null): Promise<StringIndexable> {
    return new Promise((resolve, reject) => {
        NodeHttps.get(url, res => {
            let body = "";
            res.setEncoding("utf8");
            res.on('data', data => { body += data });
            res.on('end', () => { resolve(JSON.parse(body)) });
            res.on('error', error => reject(error))
        });
    })
}

function browserRequest(url: URL, options = null): Promise<StringIndexable> {
    return fetch(url.toString()).then(r => r.json())
}

export function factoryLocation(raw: RawLocation) {
    switch (raw.type) {
        case 'station':
            return Station.FromRaw(raw)
        case 'address':
            return Location.FromRaw(raw)
    }
}

export function generateUri(path: string, params?: StringIndexable, baseUri: string = BASE_URI) {
    const url = new URL(baseUri);
    url.pathname = joinPaths([url.pathname, path])
    for (const key in params) {
        url.searchParams.append(key, params[key])
    }
    return url;
}

export function uriRequest(path: string, context: Context, params?: StringIndexable) {
    const uri = generateUri(path, params, context.baseUri)
    if (context.enviroment === ContextEnviroment.Node) {
        return nodeRequest(uri)
    }
    if (context.enviroment === ContextEnviroment.Browser) {
        return browserRequest(uri)
    }

}

export function formatTime(date: Date) {
    return [date.getHours(), date.getMinutes()].join(':')
}
