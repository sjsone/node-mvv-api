
import { RawLocation } from './RawLocation'

export interface RawLocationQueryWebResult {
    locations: RawLocation[]
}